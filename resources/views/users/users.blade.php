@extends('layouts.app')
@section('title', 'All Users')
@section('content')

        

<div class="card">
  <h3 class="card-header text-center font-weight-bold text-uppercase py-4">List of Users</h3>
  <div class="card-body">

    <div id="table" class="table-editable">
      <span class="table-add float-right mb-3 mr-2"></span>


        <table class="table table-bordered table-responsive-md table-striped text-center">
        @csrf

        <thead class="thead-dark">
            <tr>

                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Department</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Created</th>
                <th>Updated</th>
            </tr>
        </thead>

            <!-- the table data -->
            @foreach($users as $user)
            <tr>

                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->department->name}}</td>
                <td ><a  class="btn btn-info btn-rounded btn-sm my-0" href= "{{ route('edit_user', $user->id) }}" >edit</a> </td>
                <td><a   class="btn btn-danger btn-rounded btn-sm my-0" href= "{{ route('delete_user', $user->id) }}">delete</a> </td>

                <td>{{$user->created_at}}</td>
                
                <td>{{$user->updated_at}}</td>

                </tr>
            @endforeach
        </table>
@endsection
