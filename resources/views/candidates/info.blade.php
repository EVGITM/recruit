@extends('layouts.app')
@section('title', 'Create Candidate')

@section('content')
            <h1 >Candidate Info</h1>
            
            <table   class="table table-bordered table-responsive-md table-striped text-center" style="width:40%">

            <thead >
            <tr>
                <th>ID</th>
                 <th>{{$candidate->id}}</th>
            </tr>
            <tr>
                <th>Name</th>
                 <th>{{$candidate->name}}</th>
            </tr>      <tr>
                <th>Age</th>
                 <th>{{$candidate->age}}</th>
            </tr>      <tr>
                <th>Email</th>
                 <th>{{$candidate->email}}</th>
            </tr>
            </tr>      
            <tr>
                <th>User</th>
                 <th>{{$candidate->user->name}}</th>
            </tr>
            <tr>
                
            <th>Status</th>
            <td>
                <div class="dropdown">
                @if(App\Status::next($candidate->status_id) !=NULL )

                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($candidate->status_id))
                            {{$candidate->status->name}}

                          @endif                    
                          </button>
                        @else
                        {{$candidate->status->name}}

                        @endif                    

                          @if(App\Status::next($candidate->status_id) !=NULL )
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach(App\Status::next($candidate->status_id) as $status)
                            <a class="dropdown-item" href=  @php $temp = $status->id)  @endphp >{{$status->name}}</a>
                            @endforeach
                        </div>
                        @endif                    
                    </div>
                </td>
{{$temp}}
            </tr>
            <div>
        </div>
        </thead>
              

        </table>

        <td ><a  class="btn btn-info btn-rounded btn-sm my-0" href= "{{action ('CandidatesController@edit', $candidate->id)}}" ><h6>UPDATE STATUS</h6></a> </td>

         
@endsection


