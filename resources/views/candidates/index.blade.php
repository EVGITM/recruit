@extends('layouts.app')
@section('title', 'All Candidates')
@section('content')

    @if(Session::has('notallowed'))
    <div class = "alert alert-danger"> 
        {{Session::get('notallowed')}} 
    </div>
    @endif                    

        

<div class="card">
  <h3 class="card-header text-center font-weight-bold text-uppercase py-4">List of Candidates</h3>
  <div class="card-body">
  <div><a class="btn btn-light" role="button" href="{{url('/candidates/create')}}">Add New Condidate</a></div>

    <div id="table" class="table-editable">
      <span class="table-add float-right mb-3 mr-2"></span>


        <table class="table table-bordered table-responsive-md table-striped text-center">
        @csrf

        <thead class="thead-dark">
            <tr>

                <th>ID</th>
                <th>Name</th>
                <th><a href="{{route('candidate.sort')}}" >Age</a></th>
                <th>Email</th>
                <th>Status</th>
                <th>User</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>info</th>
                <th>Created</th>
                <th>Updated</th>
            </tr>
        </thead>

            <!-- the table data -->
            @foreach($candidates as $candidate)
            
                @php
                $color = '#231e1e';

                if( $candidate->status_id == 5 ){
                    $color = '#2c504a';
                } elseif(($candidate->status_id == 2) or ($candidate->status_id == 4)){
                    $color = ' #ad1d2d';
                }
                @endphp

                <tr    style= 'color: white;background-color: {{$color}}'>
                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->age}}</td>
                <td>{{$candidate->email}}</td>
                <td>
                <div class="dropdown">
                @if(App\Status::next($candidate->status_id) !=NULL )

                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($candidate->status_id))
                            {{$candidate->status->name}}

                          @endif                    
                          </button>
                        @else
                        {{$candidate->status->name}}

                        @endif                    

                          @if(App\Status::next($candidate->status_id) !=NULL )
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach(App\Status::next($candidate->status_id) as $status)
                            <a class="dropdown-item" href= "{{route ('candidate.changestatus', [$candidate->id,$status->id])}}">{{$status->name}}</a>
                            @endforeach
                        </div>
                        @endif                    
                    </div>
                </td>

                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($candidate->user_id))
                            {{$candidate->user->name}}
                           
                        @else
                        Assign owner
                        @endif                   
                         </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($users as $user)
                            <a class="dropdown-item" href= "{{route ('candidate.changeuser', [$candidate->id,$user->id])}}">{{$user->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </td>
            
                <td ><a  class="btn btn-info btn-rounded btn-sm my-0" href= "{{action ('CandidatesController@edit', $candidate->id)}}" >edit</a> </td>
                <td><a class="btn btn-danger btn-rounded btn-sm my-0" href= "{{route ('candidate.delete', $candidate->id)}}">delete</a> </td>
                <td><a  href= "{{route ('candidate.info', $candidate->id)}}">check info</a> </td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>

                
            </tr>
            @endforeach
        </table>
@endsection

