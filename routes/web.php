<?php

use Illuminate\Support\Facades\Route;

Route::resource('candidates', 'CandidatesController')->middleware('auth');
Route::get('candidates/deletecandidate/{cid}', 'CandidatesController@destroy')->name('candidate.delete')->middleware('auth');;
Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser')->middleware('auth');;
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth');;

Route::get('/mycandidates', 'CandidatesController@myCandidates')->name('candidate.mycandidates')->middleware('auth');
Route::get('candidate/', 'CandidatesController@myCandidates')->name('candidate.sort')->middleware('auth');
Route::get('candidates/infocandidate/{cid}', 'CandidatesController@info')->name('candidate.info')->middleware('auth');



Route::get('add.user.form', '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('user.form')->middleware('auth');
Route::POST('add.user', '\App\Http\Controllers\Auth\RegisterController@adduser')->name('adduser')->middleware('auth');


Route::resource('users', 'UsersController')->middleware('auth');
Route::get('users/edit_user/{cid}', 'UsersController@edit')->name('edit_user')->middleware('auth');;
Route::get('users/delete/{cid}', 'UsersController@destroy')->name('delete_user')->middleware('auth');;



Route::get('/', function () {
    return view('welcome');
});
/* 
Route::get('/hello', function(){
    return 'Hello LARAVEL';
});

Route::get('/student/{id}', function($id = 'Student'){
    return 'We got stident with id:'.$id ;
});

Route::get('/car/{id?}', function($id = null){
    if (isset($id)){
        //TODO: validation for integer 
        return "we got car $id";
    }
    else {
        return 'we need the id to find your car';
    }
});


Route::get('/comment/{id}', function ($id) {
    return view('comment',compact('id'));
});



//EX 05
Route::get('/users/{email}/{name?}', function ($email = null, $name ='name missing') {
    return view('users',compact('email','name'));
});
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
